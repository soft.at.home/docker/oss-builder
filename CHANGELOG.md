# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.2.1 - 2021-09-13(14:42:29 +0000)

### Changes

- Add plantuml

## Release v0.2.0 - 2021-06-22(14:50:14 +0000)

### New

- Add pytest

## Release v0.1.0 - 2021-06-21(08:45:59 +0000)

### New

- Add pip wheel

## Release v0.0.0 - 2021-05-19(08:25:16 +0000)

### New

- Initial release
