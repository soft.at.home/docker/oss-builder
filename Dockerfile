FROM debian:buster

ENV PLANTUML_JAR_PATH=/usr/share/plantuml/plantuml.jar

RUN apt-get clean && \
    apt-get update && \
    apt-get -y --no-install-recommends install \
    alien \
    automake \
    bash-completion \
    bc \
    bison \
    build-essential \
    ca-certificates \
    chrpath  \
    clang-tools \
    curl \
    cpio  \
    cpp  \
    diffstat  \
    dirmngr \
    doxygen \
    dumb-init \
    fakeroot \
    flex \
    g++ \
    gawk \
    gcc \
    gcovr \
    gdisk \
    gettext \
    git-lfs \
    gnupg \
    gpg \
    gpg-agent \
    graphviz \
    git \
    libcmocka-dev \
    liblzo2-dev \
    libncurses5-dev \
    libssl-dev  \
    libtool \
    locales\
    m4 \
    make \
    openssh-client \
    openssh-server \
    patch \
    pkg-config \
    plantuml \
    pmccabe \
    python \
    python3 \
    python3-setuptools \
    python3-pip \
    rsync \
    ssh \
    sudo \
    swig \
    texinfo \
    time \
    tree \
    uncrustify \
    unzip \
    uuid-dev \
    valgrind \
    wget \
    zlib1g-dev

RUN pip3 install pyyaml wheel pytest

RUN echo "dash dash/sh boolean false" | debconf-set-selections && \
    DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

COPY resources/SoFa_09C2ED61_Public.asc .
RUN cat SoFa_09C2ED61_Public.asc | apt-key add - && \
    echo "deb https://nexus.rd.softathome.com/repository/apt sah main" >> \
    /etc/apt/sources.list.d/softathome.list && \
    apt update && \
    rm SoFa_09C2ED61_Public.asc

RUN curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
RUN chmod a+x /usr/local/bin/repo

# Set the locale, else yocto will complain
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen && \
    update-locale LANG=en_US.UTF-8

ENV TERM xterm-256color
ENV LANG=C.UTF-8

ARG user=sahoss
RUN groupadd $user && useradd -m -g $user $user
RUN echo "$user ALL=(ALL) NOPASSWD: ALL" | tee -a /etc/sudoers

USER $user

WORKDIR /home/sahoss