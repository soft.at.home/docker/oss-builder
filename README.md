# Build environment

This docker container image provides an environment for building SoftAtHome components. A prebuild version of this dockerfile can be found at [Docker Hub](https://hub.docker.com/r/softathome/oss-builder). This container is mainly used for [Gitlab CI/CD](https://gitlab.com/soft.at.home/ci/gitlab-ci). For testing, debugging and developping we recommend you to use the [debug & development container image](https://gitlab.com/soft.at.home/docker/oss-dbg).

## Gitlab CI/CD

>>>
The continuous methodologies of software development are based on automating the execution of scripts to minimize the chance of introducing errors while developing applications. They require less human intervention or even no intervention at all, from the development of new code until its deployment.

It involves continuously building, testing, and deploying code changes at every small iteration, reducing the chance of developing new code based on bugged or failed previous versions.

GitLab CI/CD is a powerful tool built into GitLab that allows you to apply all the continuous methods (Continuous Integration, Delivery, and Deployment) to your software with no third-party application or integration needed. ~[Gitlab](https://docs.gitlab.com/ee/ci/introduction/)
>>>

More information about Gitlab CI/CD can be found [here](https://docs.gitlab.com/ee/ci/).

## Example of a Gitlab CI/CD job

```
check:scan-build:
  image: softathome/oss-builder:latest
  tags: [docker]
  stage: check
  only:
    - tags
    - branches
    - merge_requests
  script:
    - |
      if [[ ! -z "$BUILD_DEPS" ]]; then
        for i in $(seq 1 5); do 
          [ $i -gt 1 ] && sleep 15; 
          sudo apt update && s=0 && break || s=$?; 
        done; (exit $s)
        sudo apt -y install $BUILD_DEPS;
      fi
    - |
      tag=$(git describe --tags) || tag=""
      export VERSION_PREFIX=${tag%v*}
      echo "VERSION_PREFIX = " $VERSION_PREFIX
      scan-build -v -plist-html -o $SA_CLANG_OUTPUT -analyzer-config stable-report-filename=true make
    - |
      subdircount=$(find $CI_PROJECT_DIR/$SA_CLANG_OUTPUT -maxdepth 1 -type d | wc -l)
      if [ $subdircount -gt 1 ]; then 
        echo "BUGS FOUND"; 
        exit 1
      fi
  allow_failure: true
  artifacts:
      paths:
        - $CI_PROJECT_DIR/$SA_CLANG_OUTPUT/*
      when: on_failure
```

The above job performs a static code analyses using clang.

The job description is in [yaml](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html) (yet another markup language).

The specific keywords for Gitlab CI/CD are documented in the [GitLab CI/CD pipeline configuration reference](https://docs.gitlab.com/ee/ci/yaml/) document.
